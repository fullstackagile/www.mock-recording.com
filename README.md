# www.mock-recording.com

Website source for https://www.mock-recording.com/

```bash
npm install
npm start
```

[Markdown reference](https://markdown-it.github.io/)

[Deployment setup](./doc/aws.md)

[DNS setup](./doc/dns.md)
