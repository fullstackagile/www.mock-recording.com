const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(syntaxHighlight);
  eleventyConfig.addPassthroughCopy('src/_style');
  eleventyConfig.setBrowserSyncConfig({
    open: 'local'
  });
  eleventyConfig.addFilter('log', value => {
    console.log('log', value)
  });
  return {
    templateFormats: [
      // Note: use liquid template engine
      'html',
      'md',
      // Note: uses passthroughFileCopy
      'gif',
      'jpg',
      'pdf',
      'png',
      'svg',
    ],
    dir: {
      input: 'src',
      layouts: "_layouts",
      output: 'public',
    },
  };
};
