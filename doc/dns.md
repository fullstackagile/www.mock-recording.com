# DNS configuration

## DNS

for mock-recording.com

- A `@` - `192.0.2.1` - enable cloudflare proxying
- CNAME `www` - `www.mock-recording.com.s3-website.eu-central-1.amazonaws.com`
- forward `mock-recording.com/*` to `www.mock-recording.com/$1`
- Always use HTTPS: `http://*.mock-recording.com/*`
