module.exports = {
  baseUrl: 'https://www.mock-recording.com',
  description: 'Learn how to use mock recording to get fast, robust and accurate tests for apps, web, services and embedded software.',
  menuPageKeyList: [
    'http',
    'ble',
    'ws',
  ],
  title: 'Mock Recording',
  subtitle: 'Sub-second integration testing'
};