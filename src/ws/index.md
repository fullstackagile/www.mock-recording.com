---
layout: page
pagekey: ws
permalink: /ws/
tags: page
title: WS
---

# Mock recording for Web Socket (WS)

There does not appear to be a lot of experience with mock recording for Web Socket traffic. For inspiration, take a look at this demo project:

- [larsthorup/ws-automock-sandbox](https://github.com/larsthorup/ws-automock-sandbox)
