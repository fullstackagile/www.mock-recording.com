---
layout: page
pagekey: http
permalink: /http/
tags: page
title: HTTP
---

# Mock recording for HTTP

Mock recording has been used for HTTP traffic for many years, and quite a lot of popular tools already exists for various platforms.

Here we will describe how to use

- [Polly.js](https://netflix.github.io/pollyjs/) - HTTP mock recording for JavaScript
- [Hoverfly](https://docs.hoverfly.io/) - HTTP mock recording for Java

Other platforms with a mock recording tool includes:

- Python: [VCR.py](https://vcrpy.readthedocs.io/)
- Ruby: [RubyVCR](https://relishapp.com/vcr/vcr/)

## Real world experience

This clip shows the speed of running more than 1500 front-end unit tests in a few seconds. The clip comes from a commercial medium-sized React application, tested with mock recording for all HTTP traffic. The engineering involved is further described in the blog post [Adding Value 32 times a week - 2 years of Continuous Deployment to Production](https://www.fullstackagile.eu/2019-08-01-continuous-deployment/).

![1553 tests passing in 4 seconds](./tz-mocha.gif)

## Diagram

With mock recording for HTTP, we usually instrument our API tests with the mock recorder, so that all the HTTP requests made by the API tests are simultaneously being recorded. This recording is then used to mock the `fetch()` method in the front-end tests.

![diagram of mock recording for HTTP](./http-mock-recording.png)

In the diagram, orange is for production code, blue is for tests and green is for data and traffic used for mocking.

## Sample code - JavaScript

With Polly.js we can record HTTP traffic by writing our API tests in JavaScript and running them in Node.js.

```js
const options: PollyConfig = {
  adapters: ["node-http"],
  persister: "fs",
  persisterOptions: { fs: { recordingsDir } },
  mode: MODES.RECORD,
};
const polly = new Polly("some-recording-name", options);

// ... do HTTP requests

polly.stop();
```

Then in our UI tests, we use Polly.js and the recording to simulate all HTTP traffic:

```js
const options: PollyConfig = {
  adapters: ["node-http"],
  persister: "fs",
  persisterOptions: { fs: { recordingsDir } },
  mode: MODES.REPLAY,
  recordIfMissing: false,
};
const polly = new Polly("some-recording-name", options);

// ... do HTTP requests

polly.stop();
```

## Sample code - Java

With Hoverfly we can record HTTP traffic by writing our API tests in Java like this:

```java
Hoverfly hoverfly = new Hoverfly(localConfigs(), HoverflyMode.CAPTURE);
hoverfly.start();

// ... do HTTP requests

hoverfly.exportSimulation("/some/path");
hoverfly.close();
```

Then in our client tests, we use Hoverfly and the recording to simulate all HTTP traffic:

```java
Hoverfly hoverfly = new Hoverfly(localConfigs(), HoverflyMode.SIMULATE);
hoverfly.start();
hoverfly.simulate(SimulationSource.file("/some/path")));

// ... do HTTP requests

hoverfly.close();
```

## Sample capture file

Polly.js stores the recording in the standard [HAR format](<https://en.wikipedia.org/wiki/HAR_(file_format)>) also used by the Network tab in browser dev tools.

A sample recording looks like this:

```json
{
  "log": {
    "_recordingName": "dream",
    "creator": {
      "name": "Polly.JS",
      "version": "4.0.4"
    },
    "entries": [
      {
        "_id": "3dd64deba4e74b1c75a3022e30ef410f",
        "request": {
          "headers": [
            {
              "name": "host",
              "value": "localhost:3011"
            }
          ],
          "method": "GET",
          "queryString": [],
          "url": "http://localhost:3011/.netlify/functions/dreams"
        },
        "response": {
          "bodySize": 70,
          "content": {
            "mimeType": "application/json",
            "size": 70,
            "text": "[{\"id\":\"2\",\"title\":\"Learn French\"},{\"id\":\"1\",\"title\":\"Visit Albania\"}]"
          },
          "headers": [
            {
              "name": "content-type",
              "value": "application/json"
            },
            {
              "name": "date",
              "value": "Tue, 25 May 2021 11:15:35 GMT"
            }
          ],
          "status": 200,
          "statusText": "OK"
        },
        "startedDateTime": "2021-05-25T11:15:35.225Z",
        "time": 243
      }
    ],
    "version": "1.2"
  }
}
```
